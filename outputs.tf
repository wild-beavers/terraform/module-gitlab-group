#####
# Group
#####

output "parent_id" {
  value = try(gitlab_group.this[0].parent_id, null)
}

output "id" {
  value = local.group_id
}

output "full_path" {
  value = try(gitlab_group.this[0].full_path, null)
}

output "full_name" {
  value = try(gitlab_group.this[0].full_name, null)
}

output "web_url" {
  value = try(gitlab_group.this[0].web_url, null)
}

#####
# Access Token
#####

output "access_tokens" {
  value     = gitlab_group_access_token.this
  sensitive = true
}
