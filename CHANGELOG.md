## 3.0.0

- maintenance: (BREAKING) requires provider 17+
- feat: (BREAKING) removes `var.default_branch_protection`. Replaced by `var.default_branch_protection_defaults`

## 2.0.0

- feat: (BREAKING) replaces deprecated `var.emails_disabled` (false) is now `var.emails_enabled` (true)
- feat: adds new `var.wiki_access_level`
- refactor: modernize project
- maintenance: (BREAKING) requires provider 16+

## 1.1.1

- refactor: removes `lookup`s without alternative values

## 1.1.0

- feat: adds `parent_id` in outputs
- test: fixes `gitlab-ci.yml` to generic terraform module

## 1.0.1

- fix: fixes project_creation_level `no one` to `noone`

## 1.0.0

- feat: first version
