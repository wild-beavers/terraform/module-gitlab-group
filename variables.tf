#####
# Group
#####

variable "group_id" {
  type        = string
  default     = null
  description = "ID of an external - already existing - group. If set, no group will be created but other characteristic like memberships or access tokens will be applied on this group."

  validation {
    condition     = var.group_id == null || try(length(var.group_id) == 8, false)
    error_message = "The var.group_id must match a valid gitlab group ID."
  }
}

variable "name" {
  type        = string
  description = "Name of this group."

  validation {
    condition     = can(regex("^[a-zA-Z0-9_][a-zA-Z0-9_ ().-]{1,253}$", var.name))
    error_message = "The var.name must match “^[a-zA-Z0-9_][a-zA-Z0-9_ ().-]{1,253}$”."
  }
}

variable "path" {
  type        = string
  description = "Path of the group."

  validation {
    condition     = can(regex("^[a-zA-Z0-9._][a-zA-Z0-9_.-]{0,252}[a-zA-Z0-9-_]$", var.path))
    error_message = "The var.name must match “^[a-zA-Z0-9_][a-zA-Z0-9_ ().-]{1,253}$”."
  }
}

variable "auto_devops_enabled" {
  type        = bool
  default     = false
  description = "Default to Auto DevOps pipeline for all projects within this group."
}

variable "avatar" {
  type        = string
  default     = null
  description = "A local path to the avatar image to upload. Note: not available for imported resources."
}

variable "default_branch_protection_defaults" {
  description = <<EOF
The default branch protection defaults:

- allow_force_push           (boolean, optional):      Allow force push for all users with push access.
- allowed_to_merge           (list(string), optional): An array of access levels allowed to merge. Valid values are: `developer`, `maintainer`, `no one`.
- allowed_to_push            (list(string), optional): An array of access levels allowed to push. Valid values are: `developer`, `maintainer`, `no one`.
- developer_can_initial_push (list(string), optional): Allow developers to initial push.
EOF
  type = object({
    allow_force_push           = optional(bool, false)
    allowed_to_merge           = optional(list(string), ["maintainer"])
    allowed_to_push            = optional(list(string), ["developer"])
    developer_can_initial_push = optional(bool, true)
  })
  default = {}
}

variable "description" {
  type        = string
  default     = null
  description = "Description of the group."
}

variable "emails_enabled" {
  type        = bool
  default     = true
  description = "Enable email notifications."
  nullable    = false
}

variable "lfs_enabled" {
  type        = bool
  default     = false
  description = "Enable/disable Large File Storage (LFS) for the projects in this group."
}

variable "membership_lock" {
  type        = bool
  default     = false
  description = "Whether users can be added to projects in this group."
}

variable "mentions_disabled" {
  type        = bool
  default     = false
  description = "Disable the capability of a group from getting mentioned."
}

variable "parent_id" {
  type        = string
  default     = null
  description = "ID of the parent group (creates a nested group)."

  validation {
    condition     = var.parent_id == null || try(length(var.parent_id) >= 8, false)
    error_message = "The var.group_id must match a valid gitlab group ID."
  }
}

variable "prevent_forking_outside_group" {
  type        = bool
  default     = false
  description = "When enabled, users can not fork projects from this group to external namespaces."
}

variable "project_creation_level" {
  type        = string
  default     = "noone"
  description = "Defaults to maintainer. Determine if developers can create projects in the group."

  validation {
    condition     = contains(["maintainer", "developer", "noone"], var.project_creation_level)
    error_message = "“var.project_creation_level” must be “maintainer”, “developer” “noone”."
  }
}

variable "request_access_enabled" {
  type        = bool
  default     = false
  description = "Allow users to request member access."
}

variable "require_two_factor_authentication" {
  type        = bool
  default     = false
  description = "Require all users in this group to setup Two-factor authentication."
}


variable "share_with_group_lock" {
  type        = bool
  default     = true
  description = "Prevent sharing a project with another group within this group."
}

variable "subgroup_creation_level" {
  type        = string
  default     = "owner"
  description = "Allowed to create subgroups."

  validation {
    condition     = contains(["maintainer", "owner"], var.subgroup_creation_level)
    error_message = "“var.subgroup_creation_level” must be “maintainer” or “owner”."
  }
}

variable "two_factor_grace_period" {
  type        = number
  default     = null
  description = "Defaults to 48. Time before Two-factor authentication is enforced (in hours)."
}

variable "visibility_level" {
  type        = string
  default     = "public"
  description = "The group's visibility. Can be `private`, `internal`, or `public`."

  validation {
    condition     = contains(["public", "internal", "private"], var.visibility_level)
    error_message = "“var.subgroup_creation_level” must be “public”, “internal” or “private”."
  }
}

variable "wiki_access_level" {
  type        = string
  description = "Set the group's wiki access level. Valid values are `disabled`, `private`, `enabled`."
  default     = "disabled"

  validation {
    condition     = contains(["disabled", "private", "enabled"], var.wiki_access_level)
    error_message = "The var.wiki_access_level must be “disabled”, “private” or “enabled”."
  }
}

#####
# Membership
#####

variable "group_memberships" {
  description = <<EOF
    Key: name: The name of the group access token.

    user_id                       (set(string), required) Id of the user.
    access_level                  (string, optional)      Access level for the member. Valid values are: `no one`, `minimal`, `guest`, `reporter`, `developer`, `maintainer`, `owner`, `master`.
    expires_at                    (string, optional)      Expiration date for the group membership. Format: `YYYY-MM-DD`. Default is `never`.
    skip_subresources_on_destroy  (bool, optional)        Expiration date for the group membership. Format: `YYYY-MM-DD`. Default is `never`.
    unassign_issuables_on_destroy (bool, optional)        Whether the removed member should be unassigned from any issues or merge requests inside a given group or project. Only used during a destroy.
EOF
  type = map(object({
    user_id                       = number
    access_level                  = optional(string, "maintainer")
    expires_at                    = optional(string, null)
    skip_subresources_on_destroy  = optional(bool, null)
    unassign_issuables_on_destroy = optional(bool, null)
  }))
  default = null
}

#####
# Labels
#####

variable "labels" {
  type = map(object({
    description = string
    color       = string
  }))
}

#####
# Access Token
#####

variable "access_tokens" {
  description = <<EOF
    Key: name: The name of the group access token.

    scopes (set(string), required)  The scope for the group access token. It determines the actions which can be performed when authenticating with this token. Valid values are: api, read_api, read_registry, write_registry, read_repository, write_repository.
    access_level (string, optional) The access level for the group access token. Valid values are: `guest`, `reporter`, `developer`, `maintainer`, `owner`.
    expires_at (string, optional)   The token expires at midnight UTC on that date. The date must be in the format YYYY-MM-DD. Default is `never`.
EOF
  type = map(object({
    scopes       = set(string)
    access_level = optional(string, null)
    expires_at   = optional(string, null)
  }))
  default = null
}

#####
# Badges
#####

variable "badges" {
  description = <<EOF
    Manage group badges

    link_url  (string, required) The image url which will be presented on group overview.
    image_url (string, required) The url linked with the badge.
EOF
  type = map(object({
    link_url  = string
    image_url = string
  }))
  default = null
}

#####
# Variables
#####

variable "variables" {
  description = <<EOF
    Manage group variables.

    key               (string, required) Name of the variable.
    value             (string, required) Value of the variable.
    protected         (bool, optional)   If set to `true`, the variable will be passed only to pipelines running on protected branches and tags.
    masked            (bool, optional)   If set to `true`, the value of the variable will be hidden in job logs. The value must meet the masking requirements.
    environment_scope (string, optional) The environment scope of the variable. Note that in Community Editions of Gitlab, values other than `*` will cause inconsistent plans.
    variable_type     (string, optional) The type of a variable. Valid values are: `env_var`, `file`.
EOF
  type = map(object({
    key               = string
    value             = string
    protected         = optional(bool, true)
    masked            = optional(bool, true)
    environment_scope = optional(string, "*")
    variable_type     = optional(string, "env_var")
  }))
  default = null
}
