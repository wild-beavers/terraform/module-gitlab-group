#####
# Group
#####

locals {
  group_id = var.group_id == null ? gitlab_group.this[0].id : var.group_id
}

resource "gitlab_group" "this" {
  for_each = var.group_id == null ? { 0 = "enabled" } : {}

  name = var.name
  path = var.path

  dynamic "default_branch_protection_defaults" {
    for_each = var.default_branch_protection_defaults != null ? [1] : []

    content {
      allow_force_push           = var.default_branch_protection_defaults.allow_force_push
      allowed_to_merge           = var.default_branch_protection_defaults.allowed_to_merge
      allowed_to_push            = var.default_branch_protection_defaults.allowed_to_push
      developer_can_initial_push = var.default_branch_protection_defaults.developer_can_initial_push
    }
  }

  auto_devops_enabled               = var.auto_devops_enabled
  avatar                            = var.avatar
  avatar_hash                       = var.avatar != null ? filesha256(var.avatar) : null
  description                       = var.description
  emails_enabled                    = var.emails_enabled
  lfs_enabled                       = var.lfs_enabled
  membership_lock                   = var.membership_lock
  mentions_disabled                 = var.mentions_disabled
  parent_id                         = var.parent_id
  prevent_forking_outside_group     = var.prevent_forking_outside_group
  project_creation_level            = var.project_creation_level
  request_access_enabled            = var.request_access_enabled
  require_two_factor_authentication = var.require_two_factor_authentication
  share_with_group_lock             = var.share_with_group_lock
  subgroup_creation_level           = var.subgroup_creation_level
  two_factor_grace_period           = var.two_factor_grace_period
  visibility_level                  = var.visibility_level
  wiki_access_level                 = var.wiki_access_level

  lifecycle {
    prevent_destroy = true
  }
}

#####
# Membership
#####

resource "gitlab_group_membership" "this" {
  for_each = var.group_memberships == null ? {} : var.group_memberships

  group_id                      = local.group_id
  user_id                       = each.value["user_id"]
  access_level                  = each.value["access_level"]
  expires_at                    = each.value["expires_at"]
  skip_subresources_on_destroy  = each.value["skip_subresources_on_destroy"]
  unassign_issuables_on_destroy = each.value["unassign_issuables_on_destroy"]
}

#####
# Labels
#####

resource "gitlab_group_label" "this" {
  for_each = var.labels

  group = local.group_id

  name        = each.key
  description = each.value["description"]
  color       = each.value["color"]
}

#####
# Access Token
#####

resource "gitlab_group_access_token" "this" {
  for_each = var.access_tokens == null ? {} : var.access_tokens

  group = local.group_id

  name   = each.key
  scopes = each.value["scopes"]

  access_level = each.value["access_level"]
  expires_at   = each.value["expires_at"]
}

#####
# Badges
#####

resource "gitlab_group_badge" "this" {
  for_each = var.badges == null ? {} : var.badges

  group     = local.group_id
  link_url  = each.value["link_url"]
  image_url = each.value["image_url"]
}

#####
# Variables
#####

resource "gitlab_group_variable" "example" {
  for_each = var.variables == null ? {} : var.variables

  group             = local.group_id
  key               = each.value["key"]
  value             = each.value["value"]
  protected         = each.value["protected"]
  masked            = each.value["masked"]
  environment_scope = each.value["environment_scope"]
  variable_type     = each.value["variable_type"]
}
