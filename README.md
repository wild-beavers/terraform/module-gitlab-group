terraform-module-gitlab-group

Generic module to manage gitlab groups and associated resources.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.3.0 |
| <a name="requirement_gitlab"></a> [gitlab](#requirement\_gitlab) | ~> 17.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_gitlab"></a> [gitlab](#provider\_gitlab) | ~> 17.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [gitlab_group.this](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/group) | resource |
| [gitlab_group_access_token.this](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/group_access_token) | resource |
| [gitlab_group_badge.this](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/group_badge) | resource |
| [gitlab_group_label.this](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/group_label) | resource |
| [gitlab_group_membership.this](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/group_membership) | resource |
| [gitlab_group_variable.example](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/group_variable) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_access_tokens"></a> [access\_tokens](#input\_access\_tokens) | Key: name: The name of the group access token.<br/><br/>    scopes (set(string), required)  The scope for the group access token. It determines the actions which can be performed when authenticating with this token. Valid values are: api, read\_api, read\_registry, write\_registry, read\_repository, write\_repository.<br/>    access\_level (string, optional) The access level for the group access token. Valid values are: `guest`, `reporter`, `developer`, `maintainer`, `owner`.<br/>    expires\_at (string, optional)   The token expires at midnight UTC on that date. The date must be in the format YYYY-MM-DD. Default is `never`. | <pre>map(object({<br/>    scopes       = set(string)<br/>    access_level = optional(string, null)<br/>    expires_at   = optional(string, null)<br/>  }))</pre> | `null` | no |
| <a name="input_auto_devops_enabled"></a> [auto\_devops\_enabled](#input\_auto\_devops\_enabled) | Default to Auto DevOps pipeline for all projects within this group. | `bool` | `false` | no |
| <a name="input_avatar"></a> [avatar](#input\_avatar) | A local path to the avatar image to upload. Note: not available for imported resources. | `string` | `null` | no |
| <a name="input_badges"></a> [badges](#input\_badges) | Manage group badges<br/><br/>    link\_url  (string, required) The image url which will be presented on group overview.<br/>    image\_url (string, required) The url linked with the badge. | <pre>map(object({<br/>    link_url  = string<br/>    image_url = string<br/>  }))</pre> | `null` | no |
| <a name="input_default_branch_protection_defaults"></a> [default\_branch\_protection\_defaults](#input\_default\_branch\_protection\_defaults) | The default branch protection defaults:<br/><br/>- allow\_force\_push           (boolean, optional):      Allow force push for all users with push access.<br/>- allowed\_to\_merge           (list(string), optional): An array of access levels allowed to merge. Valid values are: `developer`, `maintainer`, `no one`.<br/>- allowed\_to\_push            (list(string), optional): An array of access levels allowed to push. Valid values are: `developer`, `maintainer`, `no one`.<br/>- developer\_can\_initial\_push (list(string), optional): Allow developers to initial push. | <pre>object({<br/>    allow_force_push           = optional(bool, false)<br/>    allowed_to_merge           = optional(list(string), ["maintainer"])<br/>    allowed_to_push            = optional(list(string), ["developer"])<br/>    developer_can_initial_push = optional(bool, true)<br/>  })</pre> | `{}` | no |
| <a name="input_description"></a> [description](#input\_description) | Description of the group. | `string` | `null` | no |
| <a name="input_emails_enabled"></a> [emails\_enabled](#input\_emails\_enabled) | Enable email notifications. | `bool` | `true` | no |
| <a name="input_group_id"></a> [group\_id](#input\_group\_id) | ID of an external - already existing - group. If set, no group will be created but other characteristic like memberships or access tokens will be applied on this group. | `string` | `null` | no |
| <a name="input_group_memberships"></a> [group\_memberships](#input\_group\_memberships) | Key: name: The name of the group access token.<br/><br/>    user\_id                       (set(string), required) Id of the user.<br/>    access\_level                  (string, optional)      Access level for the member. Valid values are: `no one`, `minimal`, `guest`, `reporter`, `developer`, `maintainer`, `owner`, `master`.<br/>    expires\_at                    (string, optional)      Expiration date for the group membership. Format: `YYYY-MM-DD`. Default is `never`.<br/>    skip\_subresources\_on\_destroy  (bool, optional)        Expiration date for the group membership. Format: `YYYY-MM-DD`. Default is `never`.<br/>    unassign\_issuables\_on\_destroy (bool, optional)        Whether the removed member should be unassigned from any issues or merge requests inside a given group or project. Only used during a destroy. | <pre>map(object({<br/>    user_id                       = number<br/>    access_level                  = optional(string, "maintainer")<br/>    expires_at                    = optional(string, null)<br/>    skip_subresources_on_destroy  = optional(bool, null)<br/>    unassign_issuables_on_destroy = optional(bool, null)<br/>  }))</pre> | `null` | no |
| <a name="input_labels"></a> [labels](#input\_labels) | n/a | <pre>map(object({<br/>    description = string<br/>    color       = string<br/>  }))</pre> | n/a | yes |
| <a name="input_lfs_enabled"></a> [lfs\_enabled](#input\_lfs\_enabled) | Enable/disable Large File Storage (LFS) for the projects in this group. | `bool` | `false` | no |
| <a name="input_membership_lock"></a> [membership\_lock](#input\_membership\_lock) | Whether users can be added to projects in this group. | `bool` | `false` | no |
| <a name="input_mentions_disabled"></a> [mentions\_disabled](#input\_mentions\_disabled) | Disable the capability of a group from getting mentioned. | `bool` | `false` | no |
| <a name="input_name"></a> [name](#input\_name) | Name of this group. | `string` | n/a | yes |
| <a name="input_parent_id"></a> [parent\_id](#input\_parent\_id) | ID of the parent group (creates a nested group). | `string` | `null` | no |
| <a name="input_path"></a> [path](#input\_path) | Path of the group. | `string` | n/a | yes |
| <a name="input_prevent_forking_outside_group"></a> [prevent\_forking\_outside\_group](#input\_prevent\_forking\_outside\_group) | When enabled, users can not fork projects from this group to external namespaces. | `bool` | `false` | no |
| <a name="input_project_creation_level"></a> [project\_creation\_level](#input\_project\_creation\_level) | Defaults to maintainer. Determine if developers can create projects in the group. | `string` | `"noone"` | no |
| <a name="input_request_access_enabled"></a> [request\_access\_enabled](#input\_request\_access\_enabled) | Allow users to request member access. | `bool` | `false` | no |
| <a name="input_require_two_factor_authentication"></a> [require\_two\_factor\_authentication](#input\_require\_two\_factor\_authentication) | Require all users in this group to setup Two-factor authentication. | `bool` | `false` | no |
| <a name="input_share_with_group_lock"></a> [share\_with\_group\_lock](#input\_share\_with\_group\_lock) | Prevent sharing a project with another group within this group. | `bool` | `true` | no |
| <a name="input_subgroup_creation_level"></a> [subgroup\_creation\_level](#input\_subgroup\_creation\_level) | Allowed to create subgroups. | `string` | `"owner"` | no |
| <a name="input_two_factor_grace_period"></a> [two\_factor\_grace\_period](#input\_two\_factor\_grace\_period) | Defaults to 48. Time before Two-factor authentication is enforced (in hours). | `number` | `null` | no |
| <a name="input_variables"></a> [variables](#input\_variables) | Manage group variables.<br/><br/>    key               (string, required) Name of the variable.<br/>    value             (string, required) Value of the variable.<br/>    protected         (bool, optional)   If set to `true`, the variable will be passed only to pipelines running on protected branches and tags.<br/>    masked            (bool, optional)   If set to `true`, the value of the variable will be hidden in job logs. The value must meet the masking requirements.<br/>    environment\_scope (string, optional) The environment scope of the variable. Note that in Community Editions of Gitlab, values other than `*` will cause inconsistent plans.<br/>    variable\_type     (string, optional) The type of a variable. Valid values are: `env_var`, `file`. | <pre>map(object({<br/>    key               = string<br/>    value             = string<br/>    protected         = optional(bool, true)<br/>    masked            = optional(bool, true)<br/>    environment_scope = optional(string, "*")<br/>    variable_type     = optional(string, "env_var")<br/>  }))</pre> | `null` | no |
| <a name="input_visibility_level"></a> [visibility\_level](#input\_visibility\_level) | The group's visibility. Can be `private`, `internal`, or `public`. | `string` | `"public"` | no |
| <a name="input_wiki_access_level"></a> [wiki\_access\_level](#input\_wiki\_access\_level) | Set the group's wiki access level. Valid values are `disabled`, `private`, `enabled`. | `string` | `"disabled"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_access_tokens"></a> [access\_tokens](#output\_access\_tokens) | n/a |
| <a name="output_full_name"></a> [full\_name](#output\_full\_name) | n/a |
| <a name="output_full_path"></a> [full\_path](#output\_full\_path) | n/a |
| <a name="output_id"></a> [id](#output\_id) | n/a |
| <a name="output_parent_id"></a> [parent\_id](#output\_parent\_id) | n/a |
| <a name="output_web_url"></a> [web\_url](#output\_web\_url) | n/a |
<!-- END_TF_DOCS -->
